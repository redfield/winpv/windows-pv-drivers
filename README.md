# windows-pv-drivers

The build process is a WIP, but an installer can be built using these instructions.

## Automatic Provisioning

The `provisioning` directory contains files to provision a Windows builder using Boxstarter and Chocolatey. It is intended to be used on a fresh installation of Windows 10 1809.

1. Run provision.ps1 (you'll need a administrative Powershell prompt with `Set-ExecutionPolicy unrestricted -f`).
2. Reboot
3. Run install-vsix.ps1 (same requirements as above)

## Manual Provisioning

Provision a Windows machine with Visual Studio 2017, WDK for Windows 10, Python 3, Git, Wix 3.11, and the Wix extension for Visual Studio.

Set the following environment variables:
- VS: set to base of VS installation (e.g. C:\Program Files (x86)\Microsoft Visual Studio\2017\Community).
- KIT: set to base of the WDK (e.g. C:\Program Files (x86)\Windows Kits\10).
- KIT_BIN: set to bin directory in WDK (e.g. C:\Program Files (x86)\Windows Kits\10\bin\10.0.17134.0\x64)
- SYMBOL_SERVER: set to any location where driver symbols can be stored (e.g. C:\Symbols).

## Building

Clone windows-pv-drivers. Run `git submodule init` followed by `git submodule update`.

To build the drivers and installer, run build_all.py with the same parameters you would pass to build.py when building the PV drivers [(as described here)](https://wiki.xenproject.org/wiki/Windows_PV_Drivers/Building). E.g. to make checked builds without running the Static Driver Verifier, run `build_all.py checked nosdv`.

## Installing

Install XenTools.msi on a Windows 10 guest with testsigning enabled (`bcdedit /set testsigning on`).
