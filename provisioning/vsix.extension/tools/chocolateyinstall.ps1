﻿$wdkVsixArgs = @{
  PackageName   = 'wdk-vsix'
  File          = "${env:ProgramFiles(x86)}\Windows Kits\10\Vsix\WDK.vsix"
}

$wixVsixArgs = @{
  PackageName   = 'wix-vsix'
  VsixUrl       = 'https://robmensching.gallerycdn.vsassets.io/extensions/robmensching/wixtoolsetvisualstudio2017extension/0.9.21.62588/1494013210879/250616/4/Votive2017.vsix'
  Checksum      = '523f8588175e28b036a6370dcb2fd66b1dd303cf75f3f841db129f64a20364bd'
  ChecksumType  = 'sha256'
}
 
Install-VisualStudioVsixExtension @wdkVsixArgs
Install-VisualStudioVsixExtension @wixVsixArgs
