import sys
import os
from subprocess import call

def find(name, path):
    for root, dirs, files in os.walk(path):
        if name in files:
            return os.path.join(root, name)

def build_driver(driver):
    old_dir = os.getcwd()
    os.chdir(driver)
    call(["python", "build.py"] + [sys.argv[1]] + [sys.argv[2]])
    os.chdir(old_dir)

def build_drivers():
    drivers = ["xenbus", "xencons", "xeniface", "xenhid", "xenvkbd", "xenvif", "xennet", "xenvbd"]

    for driver in drivers:
        build_driver(driver)

def build_installer():
    configMap = { "checked": "Debug", "free": "Release" }
    configuration = configMap[sys.argv[1]]
    vcvarsall= find('vcvarsall.bat', os.environ['VS'])
    os.environ['MSBUILD_CONFIGURATION'] = configuration
    os.environ['MSBUILD_FILE'] = "installer.sln"
    os.environ['MSBUILD_VCVARSALL'] = vcvarsall
    call("msbuild.bat")

def build_ac97():
    configMap = { "checked": "Win8.1 Debug", "free": "Win8.1 Release" }
    configuration = configMap[sys.argv[1]]
    vcvarsall= find('vcvarsall.bat', os.environ['VS'])
    os.environ['MSBUILD_CONFIGURATION'] = configuration
    os.environ['MSBUILD_FILE'] = "ac97/C++/ac97.sln"
    os.environ['MSBUILD_VCVARSALL'] = vcvarsall
    call("msbuild.bat")

build_drivers()
build_ac97()
build_installer()
